//jQuery(function () {
//var jQuerywin = jQuery(window);
//
//jQuery('body').each(function () {
//var scroll_speed = -5;
//var jQuerythis = jQuery(this);
//	jQuery(window).scroll(function () {
//		var bgScroll = -((jQuerywin.scrollTop() - jQuerythis.offset().top) / scroll_speed);
//		var bgPosition = 'center ' + bgScroll + 'px';
//		jQuerythis.css({
//			backgroundPosition: bgPosition
//			});
//		});
//	});
//});

$(function(){
    jQuery(window).scroll(function () {
        var x = jQuery(this).scrollTop();
        jQuery('body').css('background-position', '100% ' + parseInt(-x / 5) + 'px' + ', 0% ' + parseInt(-x / 8) + 'px, 0% ' + parseInt(-x / 5) + 'px, 0% ' + parseInt(-x / 8) + 'px, center top');
        
    });
    
    $(".swiper-container").each(function(){

        var obj = $(this);
        var slides = obj.find(".swiper-slide");

        var loop = true;
        var autoplay = obj.attr("data-autoplay") ? parseInt( obj.attr("data-autoplay") ) : 5000;
        if( slides.length == 1 ){
            obj.find(".swiper-button-next, .swiper-button-prev, .swiper-pagination").remove();
            return false;
        }

        var mySwiper = new Swiper (obj, {
            // Optional parameters
            loop: loop,
            autoplay: autoplay,
            nextButton: ".swiper-button-next",
            prevButton: ".swiper-button-prev:first",
            pagination: ".swiper-pagination",
        });
    });

});

jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});